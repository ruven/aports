# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ovos-audio
pkgver=0.2.0
pkgrel=0
pkgdesc="ovos-core metapackage for audio daemon"
url="https://github.com/OpenVoiceOS/ovos-audio"
arch="noarch !s390x !loongarch64" # blocked by py3-ovos-ocp-audio-plugin
license="Apache-2.0"
depends="
	py3-importlib-metadata
	py3-ovos-bus-client
	py3-ovos-config
	py3-ovos-ocp-audio-plugin
	py3-ovos-ocp-files-plugin
	py3-ovos-ocp-m3u-plugin
	py3-ovos-ocp-news-plugin
	py3-ovos-ocp-rss-plugin
	py3-ovos-plugin-manager
	py3-ovos-tts-server-plugin
	py3-ovos-utils
	sox
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-pytest
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/OpenVoiceOS/ovos-audio/archive/refs/tags/$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -k "not test_audio_service_queue_methods" \
		--deselect test/unittests/test_end2end.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
374e78267e42dbf063ec6891b12475dbf32bb4de453b8f87b089c0dcdb71e3e42810599a3349d30b432bc5bd6777fca60188bfdb6c1af68b7915a44eb617d7eb  ovos-audio-0.2.0.tar.gz
"
