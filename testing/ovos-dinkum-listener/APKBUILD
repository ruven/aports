# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ovos-dinkum-listener
pkgver=0.1.3
pkgrel=0
pkgdesc="ovos-listener based on the voice loop from mycroft-dinkum"
url="https://github.com/OpenVoiceOS/ovos-dinkum-listener"
# 32-bit arches blocked by py3-webrtcvad -> py3-ovos-vad-plugin-webrtcvad
# s390x, ppc64le and riscv64 blocked by py3-webrtcvad -> py3-ovos-vad-plugin-webrtcvad
# loongarch64: blocked by py3-ovos-plugin-manager
arch="noarch !x86 !armhf !armv7 !s390x !ppc64le !riscv64 !loongarch64"
license="Apache-2.0"
depends="
	py3-ovos-backend-client
	py3-ovos-bus-client
	py3-ovos-config
	py3-ovos-microphone-plugin-alsa
	py3-ovos-plugin-manager
	py3-ovos-utils
	py3-ovos-vad-plugin-webrtcvad
	py3-speechrecognition
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/OpenVoiceOS/ovos-dinkum-listener/archive/refs/tags/$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest \
		--deselect test/unittests/test_service.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d26c53371fe658a89e5cc802baabceed74d8ce5f780fa72d2f4ae20c40c0dd662e8d20aeb2857cb20eb1931fda5c828dc42ef69cadedc99069306502c70a955a  ovos-dinkum-listener-0.1.3.tar.gz
"
