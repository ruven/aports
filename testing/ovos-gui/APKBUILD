# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ovos-gui
pkgver=0.2.0
pkgrel=0
pkgdesc="ovos-core metapackage for gui daemon"
url="https://github.com/OpenVoiceOS/ovos-gui"
# loongarch64: blocked by py3-ovos-plugin-manager
arch="noarch !loongarch64"
license="Apache-2.0"
depends="
	py3-ovos-bus-client
	py3-ovos-config
	py3-ovos-plugin-manager
	py3-ovos-utils
	py3-tornado
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/OpenVoiceOS/ovos-gui/archive/refs/tags/$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
bc7816086dd683fb5e6484ef987040101a8eb555f2e1e151c44b3a93f9083c893b0991f0463035bc047e53b5111794e4502738089154a8b442c3481d90040678  ovos-gui-0.2.0.tar.gz
"
