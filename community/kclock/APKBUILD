# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kclock
pkgver=24.08.0
pkgrel=0
pkgdesc="Clock app for Plasma Mobile"
url="https://invent.kde.org/utilities/kclock"
# armhf blocked by qt6-qtdeclarative
# x86 broken
arch="all !armhf !x86"
license="LicenseRef-KDE-Accepted-GPL"
depends="
	kirigami-addons
	kirigami
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami-dev
	knotifications-dev
	kstatusnotifieritem-dev
	libplasma-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtmultimedia-dev
	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/kclock.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kclock-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7a89e4f95d0eb2eb1fb7508ed4250c277f939be43ed4b623b106b2916c61780afb8b66b55962f7018db1ebf516fdd38b56e104536a0ed8cbfd243f8e18a95e61  kclock-24.08.0.tar.xz
"
