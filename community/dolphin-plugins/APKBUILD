# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=dolphin-plugins
pkgver=24.08.0
pkgrel=0
pkgdesc="Extra Dolphin plugins"
url="https://www.kde.org/applications/system/dolphin/"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later"
makedepends="
	dolphin-dev
	extra-cmake-modules
	ki18n-dev
	kio-dev
	ktexteditor-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/sdk/dolphin-plugins.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/dolphin-plugins-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_svn=OFF # Broken "error: expected unqualified-id before '(' token"
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8d72253ecf3b44b4a81ff4ab952913683b2b679220620f153dae4b045e008faccc768e40e48d6dbf41a49a15409e4fa728feee30decbf2a4a35741bf9db87fa3  dolphin-plugins-24.08.0.tar.xz
"
