# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kirigami-gallery
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/org.kde.kirigami2.gallery"
pkgdesc="Gallery application built using Kirigami"
license="LGPL-2.0-or-later"
depends="
	kirigami
	kitemmodels
	"
makedepends="
	extra-cmake-modules
	kirigami-dev
	kitemmodels-dev
	qt6-qtbase-dev
	qt6-qtsvg-dev
	qt6-qttools-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/sdk/kirigami-gallery.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kirigami-gallery-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
802fbce224d2551264c0924ae85aed61506cf08a2dd8c3351ac96dce1dd30bcfec1a18b4077be3ab742ab90b3614f43226983d134d210b5c610b05324f575d54  kirigami-gallery-24.08.0.tar.xz
"
