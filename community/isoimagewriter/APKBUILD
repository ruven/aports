# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=isoimagewriter
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/ISOImageWriter"
pkgdesc="A program to write hybrid ISO files onto a USB disk"
license="GPL-3.0-only"
makedepends="
	eudev-dev
	extra-cmake-modules
	gpgme-dev
	kauth-dev
	kcoreaddons-dev
	kcrash-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	qt6-qtbase-dev
	samurai
	solid-dev
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/isoimagewriter.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/isoimagewriter-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3436aac6cb6b18acd541d8e0abbe4262aba62957767e1522f0fa616508cb6e978881bf1f9565e738a7c5a3502cc90f9acd4f6ab3a0dd70f6b37c92480278bbe9  isoimagewriter-24.08.0.tar.xz
"
