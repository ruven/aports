# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkcompactdisc
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="Library for interfacing with CDs"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	ki18n-dev
	phonon-dev
	qt6-qtbase-dev
	samurai
	solid-dev
	"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/libkcompactdisc.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkcompactdisc-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7e52b373fb402662d8c856a972b2862e3e8474a29c3e1e0d27cef5b633aea01906357c178f5b4c801ff71c7e69c88da9a49c89128382d0f5881c4e7ae3cabe91  libkcompactdisc-24.08.0.tar.xz
"
