# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kollision
pkgver=24.08.0
pkgrel=0
pkgdesc="A simple ball dodging game"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/kollision/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/games/kollision.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kollision-$pkgver.tar.xz"
# No tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
812ae99e153175aeec1acc431cc2fb43cd598f5d4802fe726c2cd0bddc074065124d88d5ba59eea35c3b969bc3881d32f97be05871c976e8ce4310e01ed738d0  kollision-24.08.0.tar.xz
"
