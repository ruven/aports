# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdesdk-kio
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development"
pkgdesc="KIO-Slaves"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kio-dev
	perl-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/sdk/kdesdk-kio.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdesdk-kio-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
01fc26b2a4d5efc78e19d54977af9701becb091900c0103df1db4b40ea2e9c3f493a8641bcdf9f620dda2da2f5ab7658e9c0b736edf1d3cfb190fe6324e5c2b9  kdesdk-kio-24.08.0.tar.xz
"
