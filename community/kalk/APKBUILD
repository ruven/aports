# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kalk
pkgver=24.08.0
pkgrel=0
pkgdesc="A powerful cross-platfrom calculator application"
arch="all !armhf" # Blocked by qt6-qtdeclarative
url="https://invent.kde.org/utilities/kalk"
license="GPL-3.0-or-later"
makedepends="
	bison
	extra-cmake-modules
	flex
	gmp-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami-dev
	kunitconversion-dev
	libqalculate-dev
	mpfr-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/kalk.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalk-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -E "knumbertest|inputmanagertest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
404d4d0f3c17ed6c4239ba5e9deadd6691d96c80b65fdc6c4225f467672500ff783e4c9f5424dc5d65a6cce1cae2a63a90fce45f55d7b53fce1836fb1ca2c1e7  kalk-24.08.0.tar.xz
"
