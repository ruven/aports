# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libgravatar
pkgver=24.08.0
pkgrel=0
pkgdesc="KDE PIM library providing Gravatar support"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x, riscv64 and loongarch64 blocked by qt6-qtwebengine -> pimcommon
arch="all !armhf !ppc64le !s390x !riscv64 !loongarch64"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kconfig-dev
	ki18n-dev
	kio-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	pimcommon-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/libgravatar.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libgravatar-$pkgver.tar.xz"
options="net" # net required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
b78671ed0e48d8a386c4535bd0225ca9c104e7f7a2472078c7b649990a3cfb1a463fa36c456fcf070633d8020bb99df901631bbf705ec575b25fe8c66d0a9cc6  libgravatar-24.08.0.tar.xz
"
